.. Parkinson disease diagnosis with machine learning documentation master file, created by
   sphinx-quickstart on Mon Apr 20 07:32:08 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Parkinson disease diagnosis with machine learning's documentation!
=============================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
