## Parkinson's disease diagnosis using Machine Learning

Reference video: [Computer Science](https://www.youtube.com/watch?v=e0iiV9eCFQc)
Dataset: [parkinson.data](https://archive.ics.uci.edu/ml/machine-learning-databases/parkinsons/)

More about Parkinson's disease: [Mayo Clinic](https://www.mayoclinic.org/diseases-conditions/parkinsons-disease/symptoms-causes/syc-20376055)