# Get the dependencies
import pandas as pd
import  numpy as np
from xgboost import XGBClassifier
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import classification_report
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import seaborn as sns

# Load the data to a dataframe
print('Reading dataset...')
df = pd.read_csv('./dataset/parkinsons.data')
print('Finished!')
print('Dataset head:')
print(df.head())
print('Shape (rows, columns): ', df.shape)

has_disease = df['status'].value_counts()[0]
does_not_have_disease = df['status'].value_counts()[1]
print ('Number of people that has disease:', has_disease)
print ('Number of people that does not have disease:', does_not_have_disease)

percent_has_disease = has_disease/(has_disease + does_not_have_disease)*100
percent_dont_have_disease = does_not_have_disease/(has_disease + does_not_have_disease)*100

print('If I guess the individual did not have Parkinsons disease, I would be correct', percent_dont_have_disease, '% of the time.')
print('If I guess the individual has Parkinsons disease, I would be correct', percent_has_disease, '% of the time.')

# Visualize the count
sns.countplot(df['status'])
plt.show()

# Get the data types
print(df.dtypes)

# Correlation matrix
correlation_matrix = df.corr()
f, ax = plt.subplots(figsize=(12,9))
sns.heatmap(correlation_matrix, vmax=.8, square=True)
plt.show()

# Status (has or not disease) correlation matrix
k = 10 # number of variables for heatmap
cols = correlation_matrix.nlargest(k, 'status')['status'].index
cm = np.corrcoef(df[cols].values.T)
f, ax = plt.subplots(figsize=(12,9))
sns.set(font_scale=1.25)
hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size':10}, yticklabels=cols.values, xticklabels=cols.values)
plt.show()

# Create the feature data set
X = df.drop(['name', 'status'], 1)
X = np.array(X)

# Create the target data set
y = np.array(df['status'])

# Split the data into 80% training and 20% testing data sets
x_train, x_test, y_train, y_test = train_test_split(X, y, test_size = 0.2)

# Transform the feature data to be values between 0 an 1
sc = MinMaxScaler(feature_range=(0,1))
x_train = sc.fit_transform(x_train)
x_test = sc.transform(x_test)

# Create the XGBClassifier
model = XGBClassifier().fit(x_train, y_train)

# Get the models predictions
predictions = model.predict(x_test)
print('Predictions:')
print(predictions)

print('Actual results (tests):')
print(y_test)

# Get the models accuracy, precision, recall, and the f1-score
print(classification_report(y_test, predictions))
